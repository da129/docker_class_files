from ldap3 import Server, Connection, ALL
import json, sys

LDAP_SERVER = Server('ldap.duke.edu', port=636, use_ssl=True)
LDAP_CONNECTION = Connection(LDAP_SERVER)

#run an anonymous ldap query
def duke_ldap_search(netid):
  attributes=["uid","cn", "title", "eduPersonPrimaryAffiliation","eduPersonPrincipalName", "duDirJobDesc","mail","telephonenumber","givenname","cn","sn","displayname","edupersonaffiliation","duDukeId"]
  search = "({}={})".format('uid', netid)
  ldap_connection = Connection(LDAP_SERVER)
  ldap_connection.bind()
  ldap_connection.search('dc=duke,dc=edu', search, attributes=attributes)
  result = ldap_connection.entries
  ldap_connection.unbind()
  if result:
      print(json.loads(result[0].entry_to_json()))

duke_ldap_search(sys.argv[1])
